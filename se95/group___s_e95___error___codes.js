var group___s_e95___error___codes =
[
    [ "SE95_E_COMM_FAIL", "group___s_e95___error___codes.html#ga321b253f6cd9540bef41aa4a4b2f1548", null ],
    [ "SE95_E_DEV_NOT_FOUND", "group___s_e95___error___codes.html#gab74d602c4f3aa8ee08ba5a6db5d8a535", null ],
    [ "SE95_E_GPIO_FAIL", "group___s_e95___error___codes.html#ga1456996823838a12c8428ef198ebaa36", null ],
    [ "SE95_E_INVALID_LEN", "group___s_e95___error___codes.html#ga10f3573d73e321006f42ffe98658a704", null ],
    [ "SE95_E_INVALID_VAL", "group___s_e95___error___codes.html#ga30dae617c8892ddca4524de448ddd2cb", null ],
    [ "SE95_E_NULL_PTR", "group___s_e95___error___codes.html#gabd939e41cb568ff2cfddd364178224bb", null ],
    [ "SE95_E_SLEEP_MODE_FAIL", "group___s_e95___error___codes.html#gac1997ec0546a5ce29ca99f5eecf5ddd7", null ],
    [ "SE95_OK", "group___s_e95___error___codes.html#ga03587112415b6b3215676ef75d21ab68", null ]
];