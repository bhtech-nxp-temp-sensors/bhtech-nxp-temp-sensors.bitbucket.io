var group___s_e95___exported___constants =
[
    [ "SE95 Chip Identifier", "group___s_e95___chip___id.html", "group___s_e95___chip___id" ],
    [ "SE95 Slave Address", "group___s_e95___slave___address.html", "group___s_e95___slave___address" ],
    [ "SE95 Error Codes", "group___s_e95___error___codes.html", "group___s_e95___error___codes" ],
    [ "SE95 Conversion Rate", "group___s_e95___conversion___rate.html", "group___s_e95___conversion___rate" ],
    [ "SE95 OS fault queue programming", "group___s_e95___o_s___threshold.html", "group___s_e95___o_s___threshold" ],
    [ "SE95 OS polarity selection", "group___s_e95___o_s___polarity.html", "group___s_e95___o_s___polarity" ],
    [ "SE95 OS operation mode selection", "group___s_e95___o_s___mode.html", "group___s_e95___o_s___mode" ],
    [ "SE95 OS pin level", "group___s_e95___o_s___level.html", "group___s_e95___o_s___level" ],
    [ "SE95 OS line state", "group___s_e95___o_s___state.html", "group___s_e95___o_s___state" ],
    [ "SE95 operation mode selection", "group___s_e95___o_p___mode.html", "group___s_e95___o_p___mode" ],
    [ "SE95 Data resolution", "group___s_e95___data___resolution.html", "group___s_e95___data___resolution" ]
];