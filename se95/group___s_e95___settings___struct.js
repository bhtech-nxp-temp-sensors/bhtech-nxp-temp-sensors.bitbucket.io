var group___s_e95___settings___struct =
[
    [ "se95_settings_s", "structse95__settings__s.html", [
      [ "os_mode", "structse95__settings__s.html#af7720fa6ad1d7616c9e836ec3675df90", null ],
      [ "os_polarity", "structse95__settings__s.html#ac30c6d1f8fca0071bf02ee7c6c5612b0", null ],
      [ "os_threshold", "structse95__settings__s.html#aa06817cf5502fd6a69dcc8e2b6348548", null ],
      [ "rate", "structse95__settings__s.html#a788e7e096258bec32986d7b2c00adebb", null ],
      [ "thyst", "structse95__settings__s.html#ad4f29cc7f20e26ab9994925e639dd836", null ],
      [ "tos", "structse95__settings__s.html#ae50fbd6c0cdfc9ef771c781934b43468", null ]
    ] ],
    [ "se95_settings_t", "group___s_e95___settings___struct.html#gaaca7a4ee7902ddd0524bd4bd12b1c268", null ]
];