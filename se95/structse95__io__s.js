var structse95__io__s =
[
    [ "delay_ms", "structse95__io__s.html#a4f8f1b8eaf19e1c5779cdf7167eee1ae", null ],
    [ "hgpio", "structse95__io__s.html#a5e4ef1f8a287162cbc852316fc639f8e", null ],
    [ "hi2c", "structse95__io__s.html#a928d705627b219d19a8d071aeaee088b", null ],
    [ "read", "structse95__io__s.html#a15843e1e30ea39df7b040426f346514c", null ],
    [ "read_pin", "structse95__io__s.html#a34509dff22022e87c478d5e35ed7d9e5", null ],
    [ "write", "structse95__io__s.html#a7a5f071deab643ac57188912bfc753a8", null ]
];