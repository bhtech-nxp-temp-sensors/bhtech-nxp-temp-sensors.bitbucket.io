var group___s_e95___exported___functions =
[
    [ "SE95 configuration functions", "group___s_e95___conf__func.html", "group___s_e95___conf__func" ],
    [ "SE95 control functions", "group___s_e95___ctrl__func.html", "group___s_e95___ctrl__func" ],
    [ "SE95 interrupt functions", "group___s_e95___isr__func.html", "group___s_e95___isr__func" ],
    [ "SE95 comparator functions", "group___s_e95___comp__func.html", "group___s_e95___comp__func" ],
    [ "SE95 temperature functions", "group___s_e95___temp__func.html", "group___s_e95___temp__func" ],
    [ "SE95 logging functions", "group___s_e95___log__func.html", "group___s_e95___log__func" ],
    [ "SE95 version functions", "group___s_e95___ver__func.html", "group___s_e95___ver__func" ]
];