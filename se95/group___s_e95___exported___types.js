var group___s_e95___exported___types =
[
    [ "SE95 functions pointers", "group___s_e95___f_u_n_c___ptr.html", "group___s_e95___f_u_n_c___ptr" ],
    [ "SE95 IO structure", "group___s_e95___i_o___struct.html", "group___s_e95___i_o___struct" ],
    [ "SE95 settings structure", "group___s_e95___settings___struct.html", "group___s_e95___settings___struct" ],
    [ "SE95 device structure", "group___s_e95___dev___struct.html", "group___s_e95___dev___struct" ],
    [ "se95_cb_fptr_t", "group___s_e95___exported___types.html#ga33bc049d3993bd1d2dbd07482713cc3c", null ]
];